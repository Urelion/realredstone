package world.urelion.realredstone;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Powerable;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.joda.time.DateTime;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RealRedstoneListener
implements Listener {
	@Getter
	private static final RealRedstoneListener INSTANCE =
		new RealRedstoneListener();

	public void powerBlock(final Block block, final boolean power) {
		BlockData targetBlockData = block.getBlockData();
		if (targetBlockData instanceof Powerable) {
			Powerable powerable = (Powerable)targetBlockData;
			powerable.setPowered(power);
			block.setBlockData(powerable);
		}
	}

	private void onSignPowered(final Sign sign) {
		if (ExpressionManager.isRealRedstoneSign(sign)) {
			BlockData blockData = sign.getBlockData();
			if (blockData instanceof WallSign) {
				WallSign wallSign = (WallSign)blockData;

				String variableGroup = "variable";
				String operatorGroup = "operator";
				String valueGroup    = "value";

				Pattern pattern = Pattern.compile(
					"(?<" + variableGroup +
					">\\w+) ?(?<" + operatorGroup +
					">[<>]=?) ?(?<" + valueGroup + ">.*)",
					Pattern.CASE_INSENSITIVE
				);
				Matcher matcher = pattern.matcher(
					ExpressionManager.getCommand(sign)
				);

				if (matcher.matches()) {
					String variable = matcher.group(variableGroup);
					String operator = matcher.group(operatorGroup);
					String value    = matcher.group(valueGroup);

					World world = sign.getWorld();
					int   x     = sign.getX();
					int   y     = sign.getY();
					int   z     = sign.getZ();

					switch (wallSign.getFacing()) {
						case EAST:
							x -= 2;
							break;
						case NORTH:
							z += 2;
							break;
						case WEST:
							x += 2;
							break;
						case SOUTH:
							z -= 2;
							break;
						default:
							break;
					}

					boolean power = false;
					if (variable.equalsIgnoreCase("realtime")) {
						DateTime timestamp = DateTime.parse(value);

						if (operator.contains(">")) {
							power = timestamp.isBeforeNow();
						}
						if (operator.contains("<")) {
							power = power || timestamp.isAfterNow();
						}
						if (operator.contains("=")) {
							power = power || timestamp.isEqualNow();
						}
						if (operator.contains("!")) {
							power = !power;
						}
					}

					Block targetBlock = world.getBlockAt(x, y, z);
					this.powerBlock(targetBlock, power);
				}
			}
		}
	}

	private void onSignPowered(final Block block) {
		BlockState state = block.getState();
		if (state instanceof Sign) {
			Sign sign = (Sign)state;
			if (sign.getBlockData() instanceof WallSign) {
				this.onSignPowered(sign);
			}
		}
	}

	private void onBlockPowered(final Block block) {
		if (block.getType() != Material.REDSTONE_WIRE) {
			return;
		}

		World world = block.getWorld();
		int   x     = block.getX();
		int   y     = block.getY();
		int   z     = block.getZ();

		Block eastBlock = world.getBlockAt(x + 1, y, z);
		Block northBlock = world.getBlockAt(x, y, z - 1);
		Block westBlock = world.getBlockAt(x - 1, y, z);
		Block southBlock = world.getBlockAt(x, y, z + 1);

		this.onSignPowered(eastBlock);
		this.onSignPowered(northBlock);
		this.onSignPowered(westBlock);
		this.onSignPowered(southBlock);
	}

	@EventHandler
	public void onPower(final BlockRedstoneEvent event) {
		if (event.getNewCurrent() > event.getOldCurrent()) {
			Block block = event.getBlock();

			this.onBlockPowered(block);
		}
	}
}
