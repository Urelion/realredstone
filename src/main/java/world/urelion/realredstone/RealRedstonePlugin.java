package world.urelion.realredstone;

import org.bukkit.plugin.java.JavaPlugin;

public class RealRedstonePlugin
extends JavaPlugin {
	@Override
	public void onEnable() {
		super.onEnable();

		this.getServer().getPluginManager().registerEvents(
			RealRedstoneListener.getINSTANCE(),
			this
		);
	}
}
