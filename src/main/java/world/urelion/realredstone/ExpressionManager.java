package world.urelion.realredstone;

import org.bukkit.block.Sign;

import java.util.ArrayList;
import java.util.List;

public class ExpressionManager {
	public static boolean isRealRedstoneSign(final Sign sign) {
		return sign.getLine(0).equalsIgnoreCase("[RealRedstone]");
	}

	public static String getCommand(final Sign sign) {
		List<String> lines = new ArrayList<>(List.of(sign.getLines()));
		lines.remove(0);

		return String.join("", lines);
	}
}
